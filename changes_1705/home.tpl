<!DOCTYPE html>

<html>

{include file="head.tpl"} {literal}
<script>
    new WOW().init();

</script>
<style>
    input[type="email"].txtSubscribeNews {
        font-size: 1em;
        font-weight: normal;
        border: none;
        background-color: #f0f0f0;
        padding: 10px;
        width: 100%;
        color: #b09a86;
    }
    
    input.btn.btn-primary.pull-right.btnSubscribe:hover {
        background-color: #ce2029;
        background-color: #de3431;
        color: #ffffff;
    }
    
    input.btn.btn-primary.pull-right.btnSubscribe {
        margin-bottom: 20px;
    }

</style>
{/literal}


<body id="home">
    <!-- Preloader -->
    <div id="preloader">
        <div id="status"></div>
    </div>

    <!-- NavBar-->
    <header>
        <nav class="navbar-default" role="navigation">
            <div class="container-fluid">
                <!--Including Logo container TPL-->
                {include file="logo-container.tpl"}

                <div class="collapse navbar-collapse navbar-left navbar-ex1-collapse">
                    <ul class="nav navbar-nav">

                        <li class="menuItem"><a href="#whatis">Products and Solutions</a></li>
                        <li class="menuItem"><a href="#industries">Who we serve</a></li>
                        <li class="menuItem"><a href="our-team">About us</a></li>
                        <li class="menuItem"><a href="#footer">Contact us</a></li>

                    </ul>
                </div>
                <div class="collapse navbar-collapse navbar-right navbar-ex1-collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="http://tatvamanalytics.com/live.tatvamanalytics/" target="_blank" data-content="https//tatvamanalytics.com/live.tatvamanalytics/" data-type="external">Login</a></li>
                        <li><a href="#request-demo" class="hidden-xs hidden-sm " value="Request Demo">Request Demo</a></li>
                    </ul>
                </div>

            </div>
        </nav>
    </header>
    <!-- Introduction -->
    <div class="intro-header">
        <div class="col-xs-12 text-center abcen1">
            <h1 class="h1_home wow fadeIn" data-wow-delay="0.4s">TATVAM</h1>
            <h3 class="h3 wow fadeIn" data-wow-delay="0.6s">Intelligence products to enhance your customer experience</h3>
            <ul class="list-inline intro-social-buttons">
                <li id="download"><a href="#request-demo" class="btn  btn-lg mybutton_standard wow swing wow fadeIn" data-wow-delay="1.2s"><span class="network-name">Request A Demo</span></a>
                </li>
            </ul>
        </div>
        <div class="col-xs-12 text-center abcen wow fadeIn">
            <div class="button_down ">
                <a class="imgcircle wow" data-wow-duration="1.5s" href="#whatis"> <img class="img_scroll" src="{$frontend}img/icon/circle.png" alt=""> </a>
            </div>
        </div>
    </div>

    <!-- Our Offering -->
    <div id="whatis" class="content-section-b" style="border-top: 0">
        <div class="container">
            <div class="col-md-12  text-center wrap_title">
                <h2>Products and Solutions</h2>
            </div>
            <div class="row vdivide odd">
                <div class="col-sm-offset-1 col-sm-3 wow fadeInDown text-center">
                    <!--                    <a class="red-box" href="">-->
                    <div class="row">
                        <div class="font-container">
                            <span class="red-box">Customer Perception</span>
                        </div>
                        <!--                            <img class="col-xs-11 col-offset-xs-1 col-sm-12 col-md-12 col-lg-12  imgPlatform img-responsive lazy" data-original="{$frontend}img/perception.png">-->
                    </div>
                    <div class="row">
                        <p class="lead text-left">Understand customer feedback and increase customer satisfaction.</p>
                    </div>
                    <div class="row">
                        <a href="customer-perception" class="btn btn-sm btnReadMore">Read More</a>
                    </div>

                    <!--                    </a>-->
                </div>

                <div class="col-sm-offset-1 col-sm-3  wow fadeInDown text-center">
                    <!--                    <a href="">-->
                    <div class="row">
                        <div class="font-container">
                            <span class="red-box">Business Intelligence</span>
                        </div>
                        <!--                            <img class="col-xs-11 col-offset-xs-1 col-sm-12 col-md-12 col-lg-12  imgPlatform img-responsive lazy" data-original="{$frontend}img/decision.png">-->
                    </div>
                    <div class="row">
                        <p class="lead text-left">Visualize all data together to increase the bottom line</p>
                    </div>
                    <div class="row">
                        <a href="business-intelligence" class="btn btn-sm btnReadMore">Read More</a>
                    </div>
                    <!--                    </a>-->
                </div>

                <div class="col-sm-offset-1 col-sm-3 wow fadeInDown text-center">
                    <!--                    <a href="">-->
                    <div class="row">
                        <div class="font-container">
                            <span class="red-box">Advanced Analytics</span>
                        </div>
                        <!--<img class="col-xs-11 col-offset-xs-1 col-sm-12 col-md-12 col-lg-12   img-responsive lazy" data-original="{$frontend}img/business.png">-->
                    </div>
                    <div class="row">
                        <p class="lead text-left">Identify patterns in your business and prepare for the future</p>
                    </div>
                    <div class="row">
                        <a href="advanced-analytics" class="btn btn-sm btnReadMore">Read More</a>
                    </div>
                    <!--                    </a>-->

                </div>
            </div>


        </div>
    </div>
    <div id="industries">

        <div class="content-section-c white parallax-div lazy " data-original="{$frontend}img/intro/industries.jpg">
            <div class="bg-overlay indOverlay">
                <div class="row">
                    <div class="col-md-12 text-center " style="clear:both;">
                        <h2 class="industriesHeader">Who We Serve</h2>
                    </div>
                </div>
                <div class="row text-center">

                    <div class="">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3 class="parallax-text text-center col-sm-offset-2 col-sm-8 col-xs-12">
                                    We understand exactly how much value your guests are bringing you and how much joy you are working to bring them. All of our Tatvam offerings have been catered specifically to the needs of companies in the visitor serving industry
                                </h3>

                            </div>
                        </div>
                        <br/>
                        <br/>
                        <a href="whoweserve" class="btn  btn-lg mybutton_standard wow swing wow fadeIn">Read More</a>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Request a Demo -->
    <div id="request-demo" class="content-section-a paddingTop">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center wrap_title ">
                    <h2>Request A Demo</h2>
                </div>
            </div>
            <div class="row">
                <div class="alert alert-dismissible alert-success text-center" id="divFormSubmitSuccessMessage" style="display:none;">
                    <p>Your request is received, Our Team will Contact you soon</p>
                </div>
                <div class="alert alert-dismissible alert-danger text-center" id="divFormSubmitErrorMessage" style="display:none;">
                    <p>Your Request is Interrupted. Please try again after sometime</p>
                </div>
            </div>

            <img id="loadSpinner" style="display:none" />
            <div class="row">
                <form role="form" action="mail.php" method="post" id="demo">
                    <div class="col-md-6 col-xs-12 white">
                        <!-- <div class="form-group">
                            <label class="lead" for="InputTitle">Title</label>
                            <div class="input-group col-md-12 col-xs-12">
                                <select class="form-control" id="InputTitle" name="InputTitle" placeholder="Enter Title" required tabindex="1">
                                    <option >Mr</option>
                                    <option >Mrs</option>
                                    <option >Ms</option>
                                </select>
                            </div>
                        </div> -->

                        <div class="form-group">
                            <label class="lead" for="InputCompanyName">Organization Name</label>
                            <div class="input-group col-md-12 col-xs-12">
                                <input type="text" class="form-control" name="InputCompanyName" id="InputCompanyName" placeholder="Enter Company Name" tabindex="3">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="lead" for="InputPhone">Phone Number</label>
                            <div class="input-group col-md-12 col-xs-12">
                                <input class="form-control col-md-12" id="InputPhone" name="InputPhone" placeholder="Enter Phone" tabindex="5">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 white">
                        <div class="form-group">
                            <label class="lead" for="InputName">Name</label>
                            <div class="input-group col-md-12 col-xs-12">
                                <input type="text" class="form-control" name="InputName" id="InputName" placeholder="Enter Name" tabindex="2">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="lead" for="InputEmail">Email</label>
                            <div class="input-group col-md-12 col-xs-12">
                                <input type="email" class="form-control" id="InputEmail" name="InputEmail" placeholder="Enter Email" tabindex="4">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="lead" style="visibility:hidden;"> Button </label>
                            <div class="input-group col-xs-12">
                                <input type="submit" name="submit" id="submit" value="Request a Demo" class="btn btn-embossed btn-primary pull-right" tabindex="6">
                            </div>
                        </div>
                    </div>
                </form>
                <hr class="featurette-divider hidden-lg">
            </div>
        </div>
    </div>


    <!-- Success Stories -->
    {include file="testimonial.tpl"}

    <!-- Our Partners -->
    <div class="" style="padding-bottom: 60px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center wrap_title">
                    <h2>Our Partners</h2>
                </div>
                <div class="row">
                    <div class="col-sm-6 wow fadeInDown text-center">
                        <a class="red-header" href="http://www.smnetserv.com" target="open"><img class="" src="{$frontend}img/NetservLogo.png" alt="Generic placeholder image" width="150"></a>
                    </div>
                    <div class="col-sm-6 wow fadeInDown text-center">
                        <a class="red-header" href="http://www.iimb.ernet.in" target="open"><img class="" src="{$frontend}img/iimb-logo.png" alt="Generic placeholder image" width="280" style="margin-top:25px;margin-bottom:30px;"></a>
                    </div>
                </div>

            </div>
        </div>
    </div>



    <!-- Footer & Contact Us -->
    <footer id="footer">
        <div class="container content-section-c">
            <div class="row">
                <div class="col-md-12 text-center wrap_title">
                    <h2>Our Offices</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-offset-2 col-md-4 footer-banner">
                    <h4><strong>INDIA </strong></h4>

                    <ul style="padding:0;">
                        <li>5th Floor, A Block, Bagmane Laurel,</li>
                        <li>Bagmane Technology Park, C V Raman Nagar,</li>
                        <li>Bangalore - 560 093.</li>
                        <li>Phone : +(91)-(80)-40400800</li>
                        <li>Fax : +91-80-40400805</li>
                        <li>
                            E Mail :
                            <a class="lnk" href="mailto:sales@tatvamanalytics.com">sales@tatvamanalytics.com</a>
                        </li>
                    </ul>
                    <br>


                </div>

                <div class="col-md-offset-2 col-md-4 footer-banner">
                    <h4>
                        <strong>USA </strong>
                    </h4>
                    <ul>
                        <li>5755 North Point Parkway, Unit 38</li>
                        <li>Alpharetta, Atlanta, GA, 30022</li>
                        <li>Phone :+1-678-894-7305</li>
                        <li>Fax : +1-678-339-0363</li>
                        <li>Mobile : 770-713-5700</li>
                        <li>
                            E Mail :
                            <a class="lnk" href="mailto:sales@tatvamanalytics.com">sales@tatvamanalytics.com</a>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
        <div class="container" style="border-top:1px solid #5a5a5a;padding: 10px 0 30px 0;">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>Get Live Updates</h2>
                </div>
                <div class="col-md-6 col-md-offset-3 text-center">
                    <div class="mockup-content">
                        <div class="content-style-form content-style-form-4 white ">

                            <div class="alert alert-dismissible alert-success text-center" id="divSubscribeSuccessMessage" style="display:none;">
                                <p>Your are Subscribed to Our Product.</p>
                            </div>
                            <div class="alert alert-dismissible alert-danger text-center" id="divSubscribeExistMessage" style="display:none;">
                                <p>E-mail Is Already Subscribed.</p>
                            </div>
                            <div class="alert alert-dismissible alert-danger text-center" id="divSubscribeErrorMessage" style="display:none;">
                                <p>Your Request is Interrupted. Please try again after sometime.</p>
                            </div>
                            <img id="loadSpinner" style="display:none" />
                            <form id="subcsribeForm" role="form" action="mail-subscribe.php" class="error">
                                <p>
                                    <input type="email" placeholder="Enter your Email Id" name="txtSubscribeNews" id="txtSubscribeNews" class="txtSubscribeNews" required/>
                                </p>
                                <p>
                                    <input type="submit" name="submit" value="Subscribe to Our Newsletter" class="btn btn-primary pull-right btnSubscribe" style="width:100%">
                                </p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row" style="border-top:1px solid #ccc;text-align:center;padding-top:10px;">
                <p color:red>Tatvam Analytics © 2017 </p>
            </div>
        </div>
    </footer>
    <div class='container-fluid subscriber-popup'>
        <!--        To-Do: Copy the below content-->
        <section class="subscribe-toolbar">
            <div class="subscribe-toolbar__inner">

                <strong class="subscriber-close">X</strong>
                <div id="share-fb" class="subscribe-share">
                    <a href="#" class="subscribe-share__fb"> <img src="{$frontend}img/social-ico/facebook.png"></a>
                </div>
                <div id="share-tw" class="subscribe-share">
                    <a href="" class="subscribe-share__tw" target="_blank"><img src="{$frontend}img/social-ico/twitter.png"></a>
                </div>
                <div id="share-li" class="subscribe-share">
                    <a href="#" class="subscribe-share__li"><img src="{$frontend}img/social-ico/linkedin.png"></a>
                </div>

                <form class="subscribe-toolbar__form" id="js-layout-sticky-form" novalidate="novalidate">
                    <label>Join our subscribers:</label>
                    <input name="email" type="text" placeholder="Your email address">
                    <button class="btn btn-primary" type="submit">Subscribe</button>
                </form>

            </div>
        </section>
    </div>
    <div class="gotop-panel">
        <span class="gototop">
                <a href="#home"><img src="{$frontend}img/uparrow.png" alt="Go to Top" title="Go to Top" width="40"></a>
            </span>
    </div>
    <div id="button-slide">
        {include file="popupform.tpl"}
    </div>
    <div id="request-a-demo">
        <div class="button-request-demo">
            <strong>Request</strong>
            <strong>Demo</strong>
        </div>
    </div>
    </div>
    <style>


    </style>



    <!-- Smoothscroll -->
    <script type="text/javascript" src="{$frontend}js/jquery.corner.js"></script>

    <script src="{$frontend}js/wow.min.js"></script>
    {literal}
    <script>
        new WOW().init();

    </script>
    <script type="text/javascript">
        jQuery(function($) {
            jQuery(document).ready(function() {
                jQuery("#button-slide").hide();
                jQuery(".subscriber-popup").hide();
                jQuery("#request-a-demo").click(function(event) {
                    jQuery("#request-a-demo").hide();
                    jQuery("#button-slide").show(500);
                    //jQuery(".request-a-demo").css('display', 'none');
                });
                jQuery(".demo-close").click(function(event) {
                    jQuery("#button-slide").hide(500);
                    jQuery("#request-a-demo").show(500);
                });
                jQuery(".subscriber-close").click(function(event) {
                    jQuery(".subscriber-popup").remove();
                });
                jQuery("#InputPhone").keypress(function(e) {
                    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                        return false;
                    }
                    var curchr = this.value.length;
                    var curval = jQuery(this).val();
                    if (curchr == 3 && curval.indexOf("(") <= -1) {
                        jQuery(this).val("(" + curval + ")" + "-");
                    } else if (curchr == 4 && curval.indexOf("(") > -1) {
                        jQuery(this).val(curval + ")-");
                    } else if (curchr == 5 && curval.indexOf(")") > -1) {
                        jQuery(this).val(curval + "-");
                    } else if (curchr == 9) {
                        jQuery(this).val(curval + "-");
                        jQuery(this).attr('maxlength', '14');
                    }
                });

                function customValidation(personName, companyName, mailId, phoneNumber) {
                    var result = [];
                    var pnumberFilter = new RegExp(/^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/i);
                    var nameFilter = new RegExp(/^[A-Za-z\s]{1,}[\.]{0,1}[A-Za-z\s]{0,}$/i);
                    var emailFilter = new RegExp(/\b[\w\.-]+@((?!gmail|googlemail|yahoo|hotmail).)[\w\.-]+\.\w{2,4}\b/i);

                    if (!nameFilter.test(personName) || personName.length < 3) {
                        jQuery('#InputName').css('border', 'solid 2px red');
                        result.push(false);
                    } else {
                        jQuery('#InputName').css('border', 'solid 2px #bdc3c7');
                        result.push(true);
                    }
                    if (companyName.length < 2) {
                        jQuery('#InputCompanyName').css('border', 'solid 2px red');
                        result.push(false);
                    } else {
                        jQuery('#InputCompanyName').css('border', 'solid 2px #bdc3c7');
                        result.push(true);
                    }

                    if (!emailFilter.test(mailId)) {
                        jQuery('#InputEmail').css('border', 'solid 2px red');
                        result.push(false);
                    } else {
                        jQuery('#InputEmail').css('border', 'solid 2px #bdc3c7');
                        result.push(true);
                    }

                    if (!pnumberFilter.test(phoneNumber)) {
                        jQuery('#InputPhone').css('border', 'solid 2px red');
                        result.push(false);
                    } else {
                        jQuery('#InputPhone').css('border', 'solid 2px #bdc3c7');
                        result.push(true);
                    }

                    return result;
                }
                jQuery("img.lazy").lazyload();
                jQuery("#demo").submit(function(event) {
                    event.preventDefault();
                    var title = jQuery('#InputTitle').val();
                    var name = jQuery('#InputName').val();
                    var phone = jQuery('#InputPhone').val();
                    var com = jQuery('#InputCompanyName').val();
                    var email = jQuery('#InputEmail').val();
                    var dataString = '&type=sendreqdemoMail&title=' + title + '&name=' + name + '&com=' + com + '&email=' + email + '&phone=' + phone;
                    if (jQuery.inArray(false, customValidation(name, com, email, phone)) !== -1) {
                        return false
                    }
                    jQuery.ajax({
                        type: "POST",
                        data: dataString,
                        url: "ajaxgetdata.php",
                        beforeSend: function() {
                            jQuery('#loadSpinner').show();
                        },
                        complete: function() {
                            jQuery('#loadSpinner').hide();
                        },
                        success: function(res) {
                            var status = jQuery.trim(this.success.arguments[0]);
                            if (status == "Success") {
                                jQuery("#divFormSubmitSuccessMessage").show().fadeOut(5000);
                                jQuery("form").trigger("reset");

                            } else {
                                jQuery("#divFormSubmitErrorMessage").show().fadeOut(5000);
                                jQuery("form").trigger("reset");

                            }
                        },
                        error: function() {
                            jQuery("#divSubscribeSuccessMessage").hide();
                            jQuery("#divFormSubmitErrorMessage").css("visibility", "visible").fadeOut(5000);
                            jQuery("form").trigger("reset");
                        }
                    })

                    event.preventDefault();
                });

                jQuery("#subcsribeForm").submit(function(event) {
                    event.preventDefault();
                    var email = jQuery('#txtSubscribeNews').val();
                    var dataString = '&type=subscribeMail&email=' + email;
                    jQuery.ajax({
                        type: "POST",
                        data: dataString,
                        url: "ajaxgetdata.php",
                        beforeSend: function() {
                            jQuery('#loadSpinner').show();
                        },
                        complete: function() {
                            jQuery('#loadSpinner').hide();
                        },
                        success: function(res) {
                            var status = jQuery.trim(this.success.arguments[0]);
                            if (status == "Success") {
                                jQuery(".btnSubscribe").css('background-color', '#de3431');
                                jQuery("#divSubscribeSuccessMessage").show().fadeOut(5000);
                                jQuery("form").trigger("reset");

                            } else if (status == "exist") {
                                jQuery(".btnSubscribe").css('background-color', '#de3431');
                                jQuery("#divSubscribeExistMessage").show().fadeOut(5000);
                                jQuery("form").trigger("reset");
                            } else {
                                jQuery(".btnSubscribe").css('background-color', '#de3431');
                                jQuery("#divSubscribeSuccessMessage").hide();
                                jQuery("#divSubscribeErrorMessage").show().fadeOut(5000);
                                jQuery("form").trigger("reset");

                            }
                        },
                        error: function() {
                            jQuery(".btnSubscribe").css('background-color', '#de3431');
                            jQuery("#divSubscribeSuccessMessage").hide();
                            jQuery("#divSubscribeErrorMessage").css("visibility", "visible").fadeOut(5000);
                            jQuery("form").trigger("reset");

                        }
                    })
                    event.preventDefault();
                });
            });
        });

    </script>
    {/literal}
    <script src="{$frontend}js/ga.js"></script>
</body>

</html>
