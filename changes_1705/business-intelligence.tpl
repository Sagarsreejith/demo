<!doctype html>

<html>

{include file="head.tpl"} {literal}
<script>
    new WOW().init();

</script>
<style>
    .navbar-default {
        position: absolute;
        top: 0;
    }
    
    ul.biportal-features li {
        list-style-image: url('img/favicon.ico');
    }
    
    .category {
        font-size: 12px;
    }

</style>
{/literal}

<body id="home">

    <!-- Preloader -->
    <div id="preloader">
        <div id="status"></div>
    </div>

    <!-- NavBar-->
    <nav class="navbar-default" role="navigation">
        <div class="container-fluid">
            <!--Including Logo container TPL-->
            {include file="logo-container.tpl"}

            <div class="collapse navbar-collapse navbar-right navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <!-- <li class="menuItem"><a href="#businessIntelligenceHome">Page Home</a></li> -->
                    <li class="menuItem"><a href="#features">Features</a></li>
                    <li class="menuItem"><a href="#success-stories">Success Stories</a></li>
                </ul>
            </div>

        </div>
    </nav>

    <!-- Introduction -->
    <div class="row sa-intro-header text-center" id="businessIntelligenceHome">
        <!-- <div class="col-xs-1  hidden-xs">
			<h4><a onclick="document.location='home#whatis'" style="color:#fff; margin-left:25px"> Back </a></h4>
		</div> -->
        <div class="col-xs-12 ">
            <div class="page-header-container">
                <span class="page-red-box">Business Intelligence</span>
            </div>
            <h3 class="h3_home">Visualize All Data Together to Increase Your Bottom Line </h3>
            <!--
			<h2 class="h2_home">Business Intelligence</h2>
			<h3 class="h3_home">Visualize All Data Together to Increase Your Bottom Line</h3><br>
-->
        </div>
    </div>
    <div style="clear:both;">
        <div class="content-section-b">
            <div class="container">
                <div class="row">
                    <div class="col-sm-5 pull-right">
                        <img class="img-responsive pull-right" src="{$frontend}img/desktop-mobile.png" alt="">
                    </div>
                    <div class="col-sm-7">
                        <article class="lead" style="text-align:justify;">
                            <h3>Can you count on your data and make the best decisions for your business?</h3>

                            <p>Reporting can be a major challenge for many visitor serving organizations that are utilizing and collecting data from multiple applications.</p>

                            <p>We have developed an internally hosted reporting solution to provide visitor serving organizations with the standard reports and dashboards that they need most</p>

                        </article>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Product Features -->
    <div id="features">
        <div class="content-section-c ">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 text-center wrap_title" style="clear:both;">
                        <h2 class="white">Features</h2>
                    </div>
                </div>
                <div class="row">

                    <div class="col-sm-6">
                        <div class="font-container">
                            <span class="red-box">Consolidated Enterprise Data Warehouse</span>
                        </div>
                        <!--
<div class="category-container">
    <div class="category video">Consolidated Enterprise Data Warehouse</div>
</div>
-->
                        <p class="white">With every implementation of the Tatvam BI Portal, we take the time to create an Enterprise Data Warehouse that pulls all of the data you need reported on into a single environment. This not only allows us to create a single reporting ecosystem, but also means that your applications won’t get slowed down when big reports need to be pulled.</p>

                        <!--
<div class="category-container">
    <div class="category video">Secure, Internally Hosted Solution</div>
</div>
-->
                        <div class="font-container">
                            <span class="red-box">Secure, Internally Hosted Solution</span>
                        </div>
                        <p class="white">The Tatvam BI Portal can be deployed both internally on your own servers, so you don’t have to worry about any potential security risks with your visitors’ private information. Additionally, all users that are configured have secure logins to make sure nobody can access the data that isn’t supposed to.</p>
                    </div>
                    <div class="col-sm-6">
                        <div id="owl-demo-1" class="owl-carousel">
                            <img class="img-responsive" src="{$frontend}img/busIntel.jpg" alt="">
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <!--
                        <div class="category-container">
                            <div class="category video">Industry Standard Dashboards</div>
                        </div>
-->
                        <div class="font-container">
                            <span class="red-box">Industry Standard Dashboards</span>
                        </div>
                        <p class="white">
                            We understand the need to see high level information both quickly and in an easy to understand way. Our standard portal comes with pre-developed industry standard dashboards that have been proven valuable over the course of several years being used by industry leading organizations in the visitor serving industry. All dashboards are completely exportable to be furthered worked on in outside programs like excel, and new reports can always be added.</p>

                        <!--
                        <div class="category-container">
                            <div class="category video">Industry Standard Reports</div>
                        </div>
-->
                        <div class="font-container">
                            <span class="red-box">Industry Standard Dashboards</span>
                        </div>
                        <p class="white">
                            Typically, in visitor serving organizations, every department is constantly asking for specific reports to be created so they can evaluate performance in their departments. Our standard portal comes with pre-developed industry standard reports that have been proven valuable over the course of several years being used by industry leading organizations in the visitor serving industry. All reports are completely exportable to be furthered worked on in outside programs like excel, and new reports can always be added.</p>

                        <!--
                        <div class="category-container">
                            <div class="category video">Real-time Mobile Reporting</div>
                        </div>
-->
                        <div class="font-container">
                            <span class="red-box">Real-time Mobile Reporting</span>
                        </div>
                        <p class="white">
                            In many cases the need to see real-time data on your phones (i.e. like attendance numbers or how many seats are left in a show) can be a huge benefit to the people who are on the floor at your organization. Our standard Tatvam BI Portal implementation comes with a secure mobile login to ensure that you get the most pertinent information in real-time directly on your phones. </p>

                        <!--
                        <div class="category-container">
                            <div class="category video">Multi Company Permissions</div>
                        </div>
-->
                        <div class="font-container">
                            <span class="red-box">Multi Company Permissions</span>
                        </div>
                        <p class="white">
                            The Tatvam BI Portal is fully equipped to handle multiple locations in the same reporting environment. We make sure to include administrator capabilities to let you control the roles of your users, as well as ensure that all reports and dashboards can be filtered appropriately based on specific company needs.</p>

                        <!--
                        <div class="category-container">
                            <div class="category video">External Dashboard Creation</div>
                        </div>
-->
                        <div class="font-container">
                            <span class="red-box">External Dashboard Creation</span>
                        </div>
                        <p class="white">
                            While the Tatvam BI Portal provides most of what any company would need out of a comprehensive reporting environment, we understand that there is always a need for power users to build out new dashboards and reports on their own. With every installation of the Tatvam BI Portal, you receive full access and ownership of the enterprise data warehouse we develop, meaning you can plug it into any external visualization tools you want to build your own dashboards.</p>

                        <!--
<div class="category-container">
    <div class="category video">Unlimited Users and Custom Branding</div>
</div>
-->
                        <div class="font-container">
                            <span class="red-box">Unlimited Users and Custom Branding</span>
                        </div>
                        <p class="white">
                            We see the Tatvam BI Portal as a true custom tool specifically built for and owned by your company. There is no limit to the number of users you can configure to have access to your portal so long as the users are a part of your organization. We even make sure to use the colors and images that match to your company branding.</p>

                    </div>



                </div>
            </div>
        </div>
    </div>
    <!-- Success Stories -->
    {include file="testimonial.tpl"}

    <!-- Footer -->
    <footer style="clear:both;">
        <div class="container">
            <div class="row" style="text-align:center;padding-top:10px;color:#000">
                <p>Tatvam Analytics © 2017 </p>
            </div>
        </div>
    </footer>
    <div class="gotop-panel">
        <span class="gototop">
			<a href="#home"><img src="{$frontend}img/uparrow.png" alt="Go to Top" title="Go to Top" width="40"></a>
		</span>
    </div>
    <script src="{$frontend}js/ga.js"></script>
</body>

</html>
