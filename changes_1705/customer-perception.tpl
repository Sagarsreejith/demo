<!doctype html>
<html>
{include file="head.tpl"} {literal}
<script>
    $(document).ready(function() {
        new WOW().init();
    });

</script>
<style>
    .navbar-default {
        position: absolute;
        top: 0;
    }
    
    .sa-intro-header {
        padding-top: 60px;
        background-color: transparent;
    }
    
    .perceptionWorks {
        width: 200px;
        height: 150px;
    }
    
    img.img-responsive.imgValueCreated {
        width: 90%;
        margin: 0 auto;
    }
    
    .prodFeaturesMarginLeft {
        margin-left: 20px;
    }

</style>
{/literal}

<body id="home">

    <!-- Preloader -->
    <div id="preloader">
        <div id="status"></div>
    </div>

    <!-- NavBar-->
    <nav class="navbar-default" role="navigation">
        <div class="container-fluid">
            <!--Including Logo container TPL-->
            {include file="logo-container.tpl"}

            <div class="collapse navbar-collapse navbar-right navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <!-- <li class="menuItem"><a href="#customerPerceptionHome">Page Home</a></li> -->
                    <li class="menuItem"><a href="#how-it-works">Value Created</a></li>
                    <li class="menuItem"><a href="#features">Features</a></li>
                    <li class="menuItem"><a href="#success-stories">Success Stories</a></li>
                </ul>
            </div>

        </div>
    </nav>

    <!-- Introduction -->
    <div class="row sa-intro-header text-center" id="customerPerceptionHome">
        <!-- <div class="col-xs-1 hidden-xs">

            <h4><a onclick="document.location='home#whatis'" style="color:#fff; margin-left:25px"> Back </a></h4>
        </div> -->
        <div class="col-xs-12 ">
            <div class="page-header-container">
                <span class="page-red-box">Customer Perception</span>
            </div>
            <h3 class="h3_home">Understand Visitor Feedback and Increase Visitor Satisfaction </h3>
        </div>
    </div>


    <div class="content-section-a">
        <div class="container">
            <div class="row">

                <div class="col-sm-6" data-animation-delay="200">

                    <h3>Are you listening to what your customers are saying about you?</h3>

                    <p>
                        It used to be enough to just keep an eye on your customer feedback and reviews. But with billions of conversations happening daily, you need true intelligence.
                    </p>

                    <p>
                        The Tatvam Customer Perception product provides a simple user interface for you to keep track of all of your visitor feedback in one place, as well as identify trends, benchmark against similar organizations and measure sentiment at a topical level.
                    </p>

                    <p>
                        With the deep insight you take away from Tatvam, you will be able to focus on the most important things to improve your visitors' experience and ultimately drive more revenue for your organization.
                    </p>


                </div>
                <div class="col-sm-6">
                    <img class="img-responsive" src="{$frontend}img/desktoptatvam.png" alt="Generic placeholder image">
                </div>
            </div>
        </div>
    </div>

    <!-- How It Works -->
    <div id="how-it-works" class="content-section-c sa-content-section-a" style="clear:both;padding-bottom: 60px;">
        <div class="container-fluid">
            <div class="row noMargin">
                <div class="col-md-6 col-md-offset-3 text-center wrap_title white" style="clear:both;">
                    <h2>Value Created</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <img src="{$frontend}img/value-created.png" class="img-responsive imgValueCreated" />
                </div>
            </div>

        </div>
    </div>


    <!-- Product Features -->

    <div id="features">

        <div class="content-section-b">

            <div class="container">
                <div class="row ">
                    <div class="col-md-6 col-md-offset-3 text-center wrap_title" style="clear:both;">
                        <h2>Product Features</h2>
                    </div>
                </div>
                <div class="row vdivide">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="row prodFeaturesMarginLeft">
                            <div class="font-container">
                                <span class="red-box">Identify Trends</span>
                            </div>
                        </div>
                        <div class="row prodFeaturesMarginLeft">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 nopadding left">
                                <div class="text-container table-display ">
                                    <div class="table-cell-display">
                                        <!--
                                <div class="category-container">
                                    <div class="category video">Identify Trends</div>
                                </div>
-->

                                        <p class="lead text-left">
                                            Identify the most commonly mentioned words and phrases in your customer reviews, social comments and internal survey feedback. See the real numbers as to how often certain topics are getting talked about.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 nopadding right blog-photo"><img src="{$frontend}img/trend.png"></div>
                        </div>

                    </div>


                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="row prodFeaturesMarginLeft">
                            <div class="font-container">
                                <span class="red-box">Drill Into Specific Topics</span>
                            </div>
                        </div>
                        <div class="row prodFeaturesMarginLeft">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 nopadding left">
                                <div class="text-container table-display ">
                                    <div class="table-cell-display">
                                        <!--
                                <div class="category-container">
                                    <div class="category video">Identify Trends</div>
                                </div>
-->

                                        <p class="lead text-left">
                                            Drill down into the specific topics, to read the reviews that mention the topics you want to understand better &nbsp; &nbsp;&nbsp;&nbsp;
                                            <br/> <br/> <br/>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 nopadding right blog-photo"><img src="{$frontend}img/drilltopics.png"></div>
                        </div>

                    </div>
                </div>
                <!--
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 nopadding left">
    <div class="text-container table-display ">
        <div class="table-cell-display">

            <div class="category-container">
                <div class="category video">Drill Into Specific Topics</div>
            </div>

            <div class="font-container">
                <span class="red-box">Drill Into Specific Topics</span>
            </div>
            <h4 class="lead text-center">
                Drill down into the specific topics, to read the reviews that mention the topics you want to understand better &nbsp; &nbsp;&nbsp;&nbsp;
                <br/> <br/> <br/>
            </h4>
        </div>
    </div>
</div>
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 nopadding right blog-photo img-responsive xsStyle"><img src="{$frontend}img/drilltopics.png"></div>
-->

                <hr/>

                <!--
<div class="row vdivide">
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 nopadding left">
        <div class="text-container table-display ">
            <div class="table-cell-display">
                <div class="font-container">
                    <span class="red-box">Measurable Reports</span>
                </div>
                <h4 class="lead text-center">
                    See real measurable data on your customer satisfaction and work to uncover which topics are your biggest detractors and enablers.
                </h4>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 nopadding right blog-photo img-responsive xsStyle"><img src="{$frontend}img/measurablreports.png"></div>
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 nopadding left">
        <div class="text-container table-display ">
            <div class="table-cell-display">
                <div class="font-container">
                    <span class="red-box">Perception Analysis</span>
                </div>
                <h4 class="lead text-center">
                    See detailed perception scores surrounding each topic. The scoring takes into account positive and negative words that are used in context with key topics in the reviews
                </h4>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 nopadding right blog-photo img-responsive xsStyle"><img src="{$frontend}img/stars_25.png"></div>

</div>
-->
                <div class="row vdivide">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="row prodFeaturesMarginLeft">
                            <div class="font-container">
                                <span class="red-box">Measurable Reports</span>
                            </div>
                        </div>
                        <div class="row prodFeaturesMarginLeft">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 nopadding left">
                                <div class="text-container table-display ">
                                    <div class="table-cell-display">
                                        <!--
                                <div class="category-container">
                                    <div class="category video">Identify Trends</div>
                                </div>
-->

                                        <p class="lead text-left">
                                            See real measurable data on your customer satisfaction and work to uncover which topics are your biggest detractors and enablers.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 nopadding right blog-photo"><img src="{$frontend}img/measurablreports.png"></div>
                        </div>

                    </div>


                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="row prodFeaturesMarginLeft">
                            <div class="font-container">
                                <span class="red-box">Perception Analysis</span>
                            </div>
                        </div>
                        <div class="row prodFeaturesMarginLeft">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 nopadding left">
                                <div class="text-container table-display ">
                                    <div class="table-cell-display">
                                        <!--
                                <div class="category-container">
                                    <div class="category video">Identify Trends</div>
                                </div>
-->

                                        <p class="lead text-left">
                                            See detailed perception scores surrounding each topic. The scoring takes into account positive and negative words that are used in context with key topics in the reviews
                                            <br/> <br/> <br/>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 nopadding right blog-photo"><img src="{$frontend}img/stars_25.png"></div>
                        </div>

                    </div>
                </div>
                <hr/>
                <!--
<div class="row vdivide">
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 nopadding left">
        <div class="text-container table-display ">
            <div class="table-cell-display">
                <div class="font-container">
                    <span class="red-box">Industry Benchmarking</span>
                </div>
                <h4 class="lead text-center"> S ee how your organization is lining up with other similar organizations through competitor analysis and other key industry benchmark rankings.
                </h4>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 nopadding right blog-photo img-responsive xsStyle"><img src="{$frontend}img/benchmark.PNG"></div>

    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 nopadding left">
        <div class="text-container table-display ">
            <div class="table-cell-display">
                <div class="font-container">
                    <span class="red-box">One Data View</span>
                </div>
                <h4 class="lead text-center">
                    Tatvam does not just monitor your brand sentiments, but can also do the same for your competition and provide meaningful insights and comparisons. What this means is that you can track yourself vis a vis competition and take business decisions to retain your competitive edge.
                </h4>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 nopadding right blog-photo"><img src="{$frontend}img/one_data_view.png"></div>

</div>
-->
                <div class="row vdivide">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="row prodFeaturesMarginLeft">
                            <div class="font-container">
                                <span class="red-box">Industry Benchmarking</span>
                            </div>
                        </div>
                        <div class="row prodFeaturesMarginLeft">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 nopadding left">
                                <div class="text-container table-display ">
                                    <div class="table-cell-display">
                                        <!--
                                <div class="category-container">
                                    <div class="category video">Identify Trends</div>
                                </div>
-->

                                        <p class="lead text-left">
                                            See how your organization is lining up with other similar organizations through competitor analysis and other key industry benchmark rankings. </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 nopadding right blog-photo"><img src="{$frontend}img/benchmark.png"></div>
                        </div>

                    </div>


                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="row prodFeaturesMarginLeft">
                            <div class="font-container">
                                <span class="red-box">One Data View</span>
                            </div>
                        </div>
                        <div class="row prodFeaturesMarginLeft">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 nopadding left">
                                <div class="text-container table-display ">
                                    <div class="table-cell-display">
                                        <!--
                                <div class="category-container">
                                    <div class="category video">Identify Trends</div>
                                </div>
-->

                                        <p class="lead text-left">
                                            Tatvam does not just monitor your brand sentiments, but can also do the same for your competition and provide meaningful insights and comparisons. What this means is that you can track yourself vis a vis competition and take business decisions to retain your competitive edge.
                                            <br/> <br/> <br/>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 nopadding right blog-photo"><img src="{$frontend}img/one_data_view.png"></div>
                        </div>

                    </div>
                </div>
                <br/>
            </div>
        </div>
    </div>

    {include file="testimonial.tpl"}

    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row" style="text-align:center;padding-top:10px;color:#000">
                <p color:red>Tatvam Analytics © 2017 </p>
            </div>
        </div>
    </footer>

    <div class="gotop-panel">
        <span class="gototop">
            <a href="#home"><img src="{$frontend}img/uparrow.png" alt="Go to Top" title="Go to Top" width="40"></a>
        </span>
    </div>

</body>
<!-- JavaScript -->
<script src="{$frontend}js/ga.js"></script>

</html>
