<!doctype html>
<html>

{include file="head.tpl"} {literal}
<script>
    $(document).ready(function() {
        new WOW().init();
        // Handler for .ready() called.
        $('html, body').animate({
            scrollTop: $('#advancedAnalyticsHome').offset().top
        }, 'slow');
    });

</script>
<style>
    .navbar-default {
        position: absolute;
        top: 0;
    }
    
    .sa-intro-header {
        padding-top: 60px;
    }
    
    .content-section-c {
        border-bottom: 1px solid white;
    }
    
    p.lead {
        text-align: justify;
    }
    
    ul.AAFeatures li {
        list-style-image: url('{/literal}{$frontend}{literal}img/bullet_red.ico');
    }

</style>
{/literal}

<body id="home">

    <!-- Preloader -->
    <div id="preloader">
        <div id="status"></div>
    </div>

    <!-- NavBar-->
    <nav class="navbar-default" role="navigation">
        <div class="container-fluid">
            <!--Including Logo container TPL-->
            {include file="logo-container.tpl"}

            <div class="collapse navbar-collapse navbar-right navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <!-- <li class="menuItem"><a href="#advancedAnalyticsHome">Page Home</a></li> -->
                    <li class="menuItem"><a href="#analytics-types">Types</a></li>
                    <li class="menuItem"><a href="#success-stories">Success Stories</a></li>
                </ul>
            </div>

        </div>
    </nav>

    <!-- Introduction -->
    <div class="row sa-intro-header text-center" id="advancedAnalyticsHome">
        <!-- <div class="col-xs-1  hidden-xs">
            <h4><a onclick="document.location='home#whatis'" style="color:#fff; margin-left:25px"> Back </a></h4>
        </div> -->
        <div class="col-xs-12 ">
            <!--
            <h2 class="h2_home">Advanced Analytics</h2>
            <h3 class="h3_home">Identify Patterns in Your Business and Prepare for the Future </h3>
            <br>
-->
            <div class="page-header-container">
                <span class="page-red-box">Advanced Analytics</span>
            </div>
            <h3 class="h3_home">Identify Patterns in Your Business and Prepare for the Future </h3>
        </div>
    </div>
    </div>


    <!-- Types Of Analytics -->
    <div>
        <div class="content-section-b">
            <div class="container">
                <div class="row">
                    <div class="col-sm-5 pull-right">
                        <img class="img-responsive pull-right" src="{$frontend}img/analytics.jpg" alt="">
                    </div>
                    <div class="col-sm-7">

                        <h3>Are you prepared to get as much out of your data as possible?</h3>

                        <p>One of the biggest issues organizations face when working with data is not being able to pull the right insights out of the information they are collecting.</p>

                        <p>Our team of analysts and data scientists take care of the heavy lifting for you and provide simple, easy to understand insights that help you know exactly what decisions to make.</p>


                    </div>
                </div>
            </div>
        </div>
        <div class="content-section-a " id="analytics-types">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 text-center wrap_title" style="clear:both;">
                        <h2>Insights We Offer</h2>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 30px;">
                    <div class="col-sm-6 pull-left">
                        <img class="img-responsive" src="{$frontend}img/attendanceprediction.jpg" alt="">
                    </div>

                    <div class="col-sm-6">
                        <!--                        <h3 class="section-heading red-header">Attendance Prediction </h3>-->
                        <div class="font-container">
                            <span class="red-box">Attendance Prediction </span>
                        </div>
                        <p class="lead">
                            One of the best insights you can have as a visitor serving organization is an accurate picture of how many guests you should be expecting on any given day. Our attendance prediction algorithms take into account several factors including things like weather, surrounding events, exhibit closings and holidays.
                        </p>
                        With our advanced attendance prediction analytic you can:

                        <ul class="AAFeatures">
                            <li>Optimize Staff Needs</li>
                            <li>Intelligently Schedule Resources</li>
                            <li>Identify New Revenue Opportunities</li>
                            <li>Enhance Visitor Experience</li>
                            <li>Minimize Business Impact of Negative Events</li>
                        </ul>

                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-sm-6 pull-right">
                        <img class="img-responsive" src="{$frontend}img/ticketinganalysis.jpg" alt="" style="float:right">
                    </div>
                    <div class="col-sm-6">
                        <!--                        <h3 class="section-heading red-header ">Ticketing Analytics </h3>-->
                        <div class="font-container">
                            <span class="red-box">Ticketing Analytics </span>
                        </div>
                        <p class="lead">
                            Ticketing plays a major part in any organization that makes their revenue off of serving visitors. There are a lot of insights to be found and improvements that can be made as a result of analyzing the information that collects as a part of your ticketing process. With our advanced ticketing analytics you can:
                            <ul class="AAFeatures">
                                <li>Identify Customer Profiles and Optimize Ticket Offerings</li>
                                <li>Optimize Pricing Strategies</li>
                                <li>Determine Best Practices for Promotional Timing</li>
                                <li>Understand Customer Purchase Paths</li>

                            </ul>
                        </p>
                    </div>
                </div>
            </div>


            <div class="container">
                <div class="row">
                    <div class="col-sm-6 pull-left">
                        <img class="img-responsive" src="{$frontend}img/marketingandcampaign.jpg" alt="">
                    </div>
                    <div class="col-sm-6">
                        <!--                        <h3 class="section-heading  red-header">Marketing and Campaign Analytics </h3>-->
                        <div class="font-container">
                            <span class="red-box">Marketing and Campaign Analytics </span>
                        </div>
                        <p class="lead">
                            As a completely customer facing business, attendance numbers rely heavily on the marketing and campaign strategies that are implemented and run. There are massive benefits that can be realized by analyzing information with these campaigns to make sure the most impact is being realized. </p>

                        With our advanced marketing and campaign analytics you can:
                        <ul class="AAFeatures">
                            <li>Benchmark Brand Awareness and Effectiveness Against Competition</li>
                            <li> Target Customer Segments that Provide Maximum ROI</li>
                            <li> Prioritize Marketing Campaign Strategies Based off of Results</li>
                            <li>Predict Donor Attrition</li>
                            <li>Understand Campaign Conversion Rates</li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>


        <!-- Success Stories -->
        {include file="testimonial.tpl"}
    </div>

    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row" style="text-align:center;padding-top:10px;">
                <p style="color:#000;">Tatvam Analytics © 2017 </p>
            </div>
        </div>
    </footer>

    <div class="gotop-panel">
        <span class="gototop">
            <a href="#home"><img src="{$frontend}img/uparrow.png" alt="Go to Top" title="Go to Top" width="40"></a>
        </span>
    </div>
</body>
<!-- JavaScript -->
<script src="{$frontend}js/ga.js"></script>

</html>
