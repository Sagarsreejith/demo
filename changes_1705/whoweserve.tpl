<!doctype html> {include file="head.tpl"} {literal}
<style>
    .navbar-default {
        position: absolute;
        top: 0;
    }
    
    .navbar-toggle {
        display: none;
    }
    
    .sa-intro-header {
        padding-bottom: 90px;
    }
    
    .category {
        font-size: 15px;
    }
    
    article {
        padding: 20px 0;
    }
    
    section {
        padding: 10px 0;
    }
    
    ul.industries {
        list-style-image: url('img/bullet_red.ico');
    }
    
    ul.industries>li {
        padding: 10px 0;
    }

</style>
{/literal}

<body id="home">

    <!-- Preloader -->
    <div id="preloader">
        <div id="status"></div>
    </div>

    <!-- NavBar-->
    <nav class="navbar-default">
        <div class="container-fluid">
            <!--Including Logo container TPL-->
            {include file="logo-container.tpl"}

            <!-- <div class="collapse navbar-collapse navbar-right navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                <li class="menuItem"><a href="#visitorIndustry">Visitor Serving </a></li>                    
                </ul>
                </div> -->

        </div>
    </nav>

    <!-- Introduction -->
    <div class="row sa-intro-header text-center">
        <!-- <div class="col-xs-1  hidden-xs ">
            <h4><a onclick="document.location='home#industries'" style="color:#fff;margin-left:25px">Back</a></h4>
            </div> -->
        <div class="col-xs-12 text-center">
            <!--
<h2 class="h2_home">Who we serve</h2>
<br>
-->
            <div class="page-header-container">
                <span class="page-red-box">Advanced Analytics</span>
            </div>

        </div>
    </div>

    <!-- perception Analysis in Attraction Industry -->
    <div id="visitorIndustry">
        <div class="content-section-b">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-12 text-center wrap_title" style="clear:both;">
                        <h2>Visitor Serving</h2>
                    </div>
                </div>
                <div class="row">
                    <!--
                    <div class="category-container">
                        <div class="category video">Customer Perception</div>
                    </div>
-->
                    <div class="font-container">
                        <span class="red-box">Customer Perception</span>
                    </div>
                    <ul class="industries">
                        <li>Visitor Experience monitoring is a major staple of maintaining high customer satisfaction. The Tatvam perception analysis product has been built from our vast experience in the Visitor Serving Organization industry and aims to be a tool tailored to this industry.</li>
                        <li>For the Visitor serving industry , the major third party metric to look at is TripAdvisor, and even more specifically, the percentage of excellent reviews, or highest rating possible in the TripAdvisor rating system, is the most important KPI. This paired with ongoing internal visitor surveys, and around the clock social monitoring of all major social networks, gives visitor serving industry all the information they need to truly understand everything that people are saying about their organization.</li>
                        <li>Through Tatvam’s cutting edge analytics , we are now able to gain a measurable insight into what people are saying about every aspect of the attraction , i.e. specific exhibits, food, staff, cost, etc. Understanding influencers and potential impressions that get impacted from them is something that the product understands and analyses for you .Tatvam uses machine learning technologies and text analytics to not only monitor these mentions and reviews online, but take it a step further and analyze them.</section>
                    </ul>
                </div>
                <div class="row">
                    <!--
                    <div class="category-container">
                        <div class="category video">Advanced Analytics</div>
                    </div>
-->
                    <div class="font-container">
                        <span class="red-box">Advanced Analytics</span>
                    </div>
                    <ul class="industries">
                        <li><span class="lead-block red-header">Footfall Analysis :</span> Tatvam can forecast the footfalls in your store for tomorrow so that you are prepared to handle the increased crowd. You can also plan promotional marketing for the lean days so that the crowd remain balanced on peak days Vs off-peak days. The business can plan their manpower in advance based on the Tatvam’s prediction on the number of footfalls for a day.</li>
                        <li><span class="lead-block red-header">Marketing & Campaign Management :</span> Which of your customers are more likely to respond to the marketing campaigns? Which channel is more effective in providing you the sales lift? Tatvam will not only help you identify both, but it also provides a scenario planner or "what-if" analysis which helps you visualize the change in Sales with change in marketing plans. Tatvam also provides the campaign conversion rate which helps the customers know the effectiveness of a campaign.</li>
                        <li><span class="lead-block red-header"> Customer Segmentation :</span> Different industries employ different analytical techniques to segment their customers. Tatvam's customer segmentation employs a business driven approach to identify and cluster customers who are more similar to each other. Based on the customer segmentation, the business can apply personalization options for each segments.</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Footer -->
        <footer style="clear:both;">
            <div class="container">
                <div class="row" style="text-align:center;padding-top:10px;color:#000">
                    <p>Tatvam Analytics © 2017 </p>
                </div>
            </div>
        </footer>
        <div class="gotop-panel">
            <span class="gototop">
                    <a href="#home"><img src="{$frontend}img/uparrow.png" alt="Go to Top" title="Go to Top" width="40"></a>
                </span>
        </div>
        {literal}
        <script type="text/javascript">
            new WOW().init();

        </script>
        {/literal}
</body>
<!-- JavaScript -->
<script src="{$frontend}js/ga.js"></script>

</html>
